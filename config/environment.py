import os

#ENVIRONMENT = 'local'
ENVIRONMENT = 'development'
#ENVIRONMENT = 'production'

SETTINGS_MODULE = 'config.settings.local'

if 'DJANGO_SETTINGS_MODULE' in os.environ:
    SETTINGS_MODULE = os.environ['DJANGO_SETTINGS_MODULE']

elif ENVIRONMENT == 'local':
    SETTINGS_MODULE = 'config.settings.local'
elif ENVIRONMENT == 'development':
    SETTINGS_MODULE = 'config.settings.development'
elif ENVIRONMENT == 'production':
    SETTINGS_MODULE = 'config.settings.production'
