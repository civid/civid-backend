import os
import sys
from PIL import Image


def make_file_name(width, original_file):
    file_name, extension = os.path.splitext(original_file)
    print(original_file)
    print(file_name, "-", width, "w", extension)
    return f"{file_name}-{width}w{extension}"


def save_responsive_images(original_file, image_sizes):
    print(image_sizes)
    try:
        im = Image.open(original_file)
        for width in image_sizes:
            height = int((width/float(im.size[0]))*im.size[1])
            img = im.resize((width, height), Image.ANTIALIAS)
            img.save(make_file_name(width, original_file), "JPEG", quality=90)
    except IOError:
        print("cannot create thumbnail for", original_file)
