ERROR = 'Error'
SUCCESS = 'Success'

# Permissions
PERMISSION_ADMINISTRATOR_REQUIRED = 'Administrator permissions needed'
PERMISSION_MODERATOR_REQUIRED = 'Moderator permissions needed'

# User roles
USER_ROLE_ADMINISTRATOR = 'administrator'
USER_ROLE_MODERATOR = 'moderator'

# Votes

VOTE_VALUE_CHOICES = (
    (1, '1'),
    (2, '2'),
    (3, '3'),
    (4, '4'),
    (5, '5'),
)

# review Status

PENDING = "1"
REJECTED = "2"
PUBLISHED = "3"
ENQUEUE = "4"
FEATURED = "5"

QUEUE_GAP = 1000

review_status = [
    (PENDING, "Pending"),
    (REJECTED, "Rejected"),
    (PUBLISHED, "Published"),
    (ENQUEUE, "Sent to queue"),
    (FEATURED, "Featured")
]
