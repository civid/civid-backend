from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from django.db import transaction
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.contenttypes.models import ContentType

from v1.trophies.models.trophy import Trophy, AwardedTrophy
from v1.trophies.serializers.trophy import (
    TrophySerializer, TrophySerializerCreate, TrophySerializerUpdate, AwardedTrophySerializer
)
from v1.utils import constants
from v1.utils.permissions import is_administrator
from v1.posts.models.post import Post, PostStatus


# trophies
class TrophyView(APIView):

    @staticmethod
    def get(request):
        trophies = Trophy.objects.all()
        return Response(TrophySerializer(trophies, many=True).data)

    @staticmethod
    def post(request):
        """Create trophy."""
        serializer = TrophySerializerCreate(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(TrophySerializer(serializer.instance).data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# trophies/{trophy_id}
class TrophyDetail(APIView):

    @staticmethod
    def get(request, trophy_id):
        trophy = get_object_or_404(Trophy, pk=trophy_id)
        return Response(TrophySerializer(trophy).data)

    @staticmethod
    def patch(request, trophy_id):
        """Update trophy."""
        trophy = get_object_or_404(Trophy, pk=trophy_id)
        serializer = TrophySerializerUpdate(trophy, data=request.data, context={'request': request}, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(TrophySerializer(serializer.instance).data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def delete(request, trophy_id):
        """Delete trophy."""
        trophy = get_object_or_404(Trophy, pk=trophy_id)
        if not is_administrator(request.user):
            return Response({
                constants.ERROR: 'Admin permissions required to delete trophies'}, status=status.HTTP_401_UNAUTHORIZED)
        trophy.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# trophies/{trophy_id}/award_for_post/{post_id}
class TrophyAwardForPost(APIView):

    @staticmethod
    def post(request, trophy_id, post_id):
        trophy = get_object_or_404(Trophy, pk=trophy_id)
        post = get_object_or_404(Post, pk=post_id)
        if post.user != request.user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        if 'userId' not in request.data:
            return Response({
                constants.ERROR: 'No user specified to award the trophy to.'}, status=status.HTTP_400_BAD_REQUEST)
        winner = get_object_or_404(get_user_model(), pk=request.data.get('userId'))
        if post.status() != PostStatus.AWARD_PENDING:
            return Response(
                {constants.ERROR: 'Trophy cannot be awarded in the context of this post.'},
                status=status.HTTP_400_BAD_REQUEST,
            )
        post_type = ContentType.objects.get_for_model(post)
        if AwardedTrophy.objects.filter(
                trophy=trophy, context_content_type=post_type.id, context_object_id=post.id).count() > 0:
            return Response(
                {constants.ERROR: 'This trophy was already awarded in the context of this post.'},
                status=status.HTTP_400_BAD_REQUEST,
            )

        with transaction.atomic():
            awarded_trophy = AwardedTrophy(user=winner, trophy=trophy, context=post)
            awarded_trophy.save()
            winner.influence += trophy.influence
            winner.save()
            # Discussion author receives influence boost too for this kind of reward.
            request.user.influence += 10
            request.user.save()

        return Response(AwardedTrophySerializer(awarded_trophy).data, status=status.HTTP_201_CREATED)


# trophies/awarded
class AwardedTrophies(APIView):

    @staticmethod
    def get(request):
        trophies_query = AwardedTrophy.objects.all()
        if 'username' in request.query_params:
            trophies_query = trophies_query.filter(user__username=request.query_params.get('username'))
        return Response(AwardedTrophySerializer(trophies_query, many=True).data)
