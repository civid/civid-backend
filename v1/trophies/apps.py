from django.apps import AppConfig


class TrophiesConfig(AppConfig):
    name = 'v1.trophies'
