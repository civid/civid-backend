from rest_framework import serializers

from v1.utils import constants
from v1.utils.permissions import is_administrator
from v1.trophies.models.trophy import Trophy, AwardedTrophy
from v1.accounts.serializers.user import UserSerializer
from v1.posts.models.post import Post
from v1.posts.serializers.post import PostSerializerFull


class TrophySerializer(serializers.ModelSerializer):

    class Meta:
        model = Trophy
        fields = '__all__'


class TrophySerializerCreate(serializers.ModelSerializer):

    class Meta:
        model = Trophy
        fields = '__all__'

    def validate(self, data):
        """Administrator permissions needed."""
        if not is_administrator(self.context['request'].user):
            raise serializers.ValidationError(constants.PERMISSION_ADMINISTRATOR_REQUIRED)
        return data


class TrophySerializerUpdate(serializers.ModelSerializer):

    class Meta:
        model = Trophy
        fields = '__all__'

    def validate(self, data):
        """Administrator permissions needed."""
        if not is_administrator(self.context['request'].user):
            raise serializers.ValidationError(constants.PERMISSION_ADMINISTRATOR_REQUIRED)
        return data


class GenericAwardedTrophyRelatedField(serializers.RelatedField):
    def to_representation(self, value):
        serializer = None
        if isinstance(value, Post):
            serializer = PostSerializerFull(value)
        return serializer.data if serializer else None


class AwardedTrophySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    user = UserSerializer()
    trophy = TrophySerializer()
    context = GenericAwardedTrophyRelatedField(read_only=True)

    class Meta:
        model = AwardedTrophy
        fields = ('id', 'user', 'trophy', 'context')
