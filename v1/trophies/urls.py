from django.conf.urls import url
from .views.trophy import TrophyView, TrophyDetail, TrophyAwardForPost, AwardedTrophies


urlpatterns = [

    url(r'^trophies$', TrophyView.as_view()),
    url(r'^trophies/awarded$', AwardedTrophies.as_view()),
    url(r'^trophies/(?P<trophy_id>[\d]+)$', TrophyDetail.as_view()),
    url(r'^trophies/(?P<trophy_id>[\d]+)/award_for_post/(?P<post_id>[\d]+)$', TrophyAwardForPost.as_view()),

]
