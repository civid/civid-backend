from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey


class Trophy(models.Model):
    title = models.CharField(max_length=30)
    influence = models.IntegerField()
    image = models.ImageField(blank=True)
    winners = models.ManyToManyField(get_user_model(), related_name='trophies_awarded', through='AwardedTrophy')

    def __str__(self):
        return self.title


class AwardedTrophy(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    trophy = models.ForeignKey(Trophy, on_delete=models.CASCADE)
    context_content_type = models.ForeignKey(ContentType, related_name='related_trophies', on_delete=models.CASCADE)
    context_object_id = models.CharField(max_length=255)
    context = GenericForeignKey('context_content_type', 'context_object_id')
