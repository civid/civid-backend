from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from v1.reports.models.report import Report
from v1.posts.models.post import Post
from v1.replies.models.post_reply import PostReply
from v1.reports.serializers.report import (ReportSerializer,
                                             ReportSerializerCreate)
from django.contrib.contenttypes.models import ContentType
from v1.utils import constants

# reports


class ReportView(APIView):


    @staticmethod
    def get(request):
        reports = Report.objects.all()
        return Response(ReportSerializer(reports, many=True).data)


    @staticmethod
    def post(request):
        """
        Create report
        """
        if hasattr(request.data, '_mutable'):
            request.data._mutable = True

        if request.data['type_name'] == 'P':
             ct = ContentType.objects.get_for_model(Post)
        elif request.data['type_name'] == 'R':
            ct = ContentType.objects.get_for_model(PostReply)

        request.data['content_type'] = ct.id
        request.data['object_id'] = request.data['content_object']['id']



        serializer = ReportSerializerCreate(
            data=request.data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response(ReportSerializer(serializer.instance).data, status=status.HTTP_201_CREATED)
        print(serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# reports/{report_id}
class ReportDetail(APIView):
    @staticmethod
    def get(request, report_id):
        """View an individual report."""
        report = get_object_or_404(Report, pk=report_id)
        return Response(ReportSerializer(report).data)

    @staticmethod
    def delete(request, report_id):
        """
        Delete report
        """

        report = get_object_or_404(Report, pk=report_id)

        if report.user != request.user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        report.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
