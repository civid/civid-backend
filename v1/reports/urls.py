from django.conf.urls import url
from .views.report import ReportView, ReportDetail


urlpatterns = [

    # Reports
    url(r'^reports$', ReportView.as_view()),
    url(r'^reports/(?P<report_id>[\d]+)$',
        ReportDetail.as_view()
        ),

]
