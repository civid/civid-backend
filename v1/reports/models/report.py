from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey


class Report(models.Model):

    POST_TYPES = (
        ('P', 'Post'),
        ('R', 'Reply'),
    )

    message = models.CharField(max_length=200)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    type_name = models.CharField(max_length=1, choices=POST_TYPES)

    def __str__(self):
        return self.message
