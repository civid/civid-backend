from rest_framework import serializers

from v1.utils import constants
from v1.utils.permissions import is_administrator
from v1.reports.models.report import Report
from v1.accounts.serializers.user import UserSerializer
from v1.posts.models.post import Post
from v1.replies.models.post_reply import PostReply
from v1.posts.serializers.post import PostSerializerFull
from v1.replies.serializers.post_reply import PostReplySerializer


class GenericReportRelatedField(serializers.RelatedField):
    def to_representation(self, value):
        serializer = None
        if isinstance(value, Post):
            serializer = PostSerializerFull(value)
        elif isinstance(value, PostReply):
            serializer = PostReplySerializer(value)
        return serializer.data if serializer else None

class ReportSerializer(serializers.ModelSerializer):
    content_object = GenericReportRelatedField(read_only=True)
    user = UserSerializer()

    class Meta:
        model = Report
        fields = '__all__'


class ReportSerializerCreate(serializers.ModelSerializer):

    class Meta:
        model = Report
        fields = '__all__'

