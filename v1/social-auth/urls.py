from django.conf.urls import url
from .views import test
from .serializers.social_serializer import exchange_token


urlpatterns = [
    url(r'^test$', test, name='test'),
    url(r'^social-login/(?P<backend>.*)/$', exchange_token, name='social-login')
]
