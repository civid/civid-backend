from django.db import models
from v1.articles.models.article import Article
from v1.articles.models.publication import Publication


class Source(models.Model):
    title = models.CharField(max_length=255)
    url = models.URLField(max_length=1024)
    date = models.DateField()
    pub = models.ForeignKey(
        Publication,
        on_delete=models.CASCADE,
        related_name="pub_articles")
    article = models.ForeignKey(
        Article,
        on_delete=models.CASCADE,
        related_name="article_sources")

    class Meta:
        default_related_name = 'sources'

    def __str__(self):
        return self.title
