from django.db import models


class Publication(models.Model):
    name = models.CharField(max_length=25)
    image = models.ImageField(blank=True)
    url_substring = models.CharField(max_length=255, blank=True)

    class Meta:
        default_related_name = 'publications'

    def __str__(self):
        return self.name
