from django.db import models
from v1.general.responsive_image import ResponsiveImage


class Article(ResponsiveImage):
    title = models.CharField(max_length=255)
    date = models.DateTimeField()
    summary = models.TextField()
    featured = models.BooleanField(default=False)
    image_url = models.URLField(blank=True, max_length=1024)

    class Meta:
        default_related_name = 'articles'

    def get_sizes(self):
        return (320, 480, 640, 769,  900)

    def thumbnail(self):
        return (50,)

    def __str__(self):
        return self.title
