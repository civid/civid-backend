from rest_framework import serializers
from v1.articles.models.source import Source
from v1.utils.permissions import is_administrator
from v1.articles.serializers.publication import PublicationSerializer


class SourceSerializer(serializers.ModelSerializer):
    pub = PublicationSerializer()

    class Meta:
        model = Source
        fields = '__all__'

class SourceSerializerCreate(serializers.ModelSerializer):

    class Meta:
        model = Source
        fields = '__all__'

    def validate(self, data):
        """Validate user as administrator."""
        if not is_administrator(self.context['request'].user):
            raise serializers.ValidationError(constants.PERMISSION_ADMINISTRATOR_REQUIRED)
        return data

class SourceSerializerUpdate(serializers.ModelSerializer):

    class Meta:
        model = Source
        fields = '__all__'

    def validate(self, data):
        """Validate user as administrator."""
        if not is_administrator(self.context['request'].user):
            raise serializers.ValidationError(constants.PERMISSION_ADMINISTRATOR_REQUIRED)
        return data

