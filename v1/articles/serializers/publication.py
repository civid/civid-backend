from rest_framework import serializers
from v1.articles.models.publication import Publication
from v1.utils.permissions import is_administrator


class PublicationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Publication
        fields = '__all__'

class PublicationSerializerCreate(serializers.ModelSerializer):

    class Meta:
        model = Publication
        fields = '__all__'

    def validate(self, data):
        """Validate user as administrator."""
        if not is_administrator(self.context['request'].user):
            raise serializers.ValidationError(constants.PERMISSION_ADMINISTRATOR_REQUIRED)
        return data

class PublicationSerializerUpdate(serializers.ModelSerializer):

    class Meta:
        model = Publication
        fields = '__all__'

    def validate(self, data):
        """Validate user as administrator."""
        if not is_administrator(self.context['request'].user):
            raise serializers.ValidationError(constants.PERMISSION_ADMINISTRATOR_REQUIRED)
        return data

