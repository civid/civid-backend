from rest_framework import serializers

from v1.articles.models.article import Article


class ArticleSerializer(serializers.ModelSerializer):
    opinions_count = serializers.SerializerMethodField()
    images_sizes = serializers.SerializerMethodField()

    class Meta:
        model = Article
        fields = '__all__'

    @staticmethod
    def get_opinions_count(obj):
        return obj.posts.count()

    @staticmethod
    def get_images_sizes(obj):
        return obj.get_sizes()


class ArticleSerializerCreate(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Article
        fields = '__all__'


class ArticleSerializerFull(ArticleSerializer):
    from v1.posts.serializers.post import PostSerializer

    posts = PostSerializer(many=True, read_only=True)


class ArticleSerializerUpdate(serializers.ModelSerializer):

    class Meta:
        model = Article
        exclude = ('user',)

    def validate(self, data):
        """Validate user as administrator."""
        if self.instance.user != self.context['request'].user:
            raise serializers.ValidationError(
                'You can not edit articles from other users')
        return data
