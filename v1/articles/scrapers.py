from datetime import date, datetime
from aylienapiclient import textapi


class MockArticleScraper(object):
    def fetch_url_data(self, url):
        return {
            'title': 'A random mocked title.',
            'summary': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'image': 'http://catsatthestudios.com/wp-content/uploads/2017/12/12920541_1345368955489850_5587934409579916708_n-2-960x410.jpg',
            'date': date.today()
        }


class AylienScraper(object):
    def fetch_url_data(self, url):
        client = textapi.Client("e8be0cd6", "2a7a1ff94aab7c14ec0dc8854d4d1989")
        extract = client.Extract({"url": url, "best_image": True})
        result = {
            'title': extract['title'],
            'summary': extract['article'],
            'image': extract['image'],
        }
        if all(len(v) == 0 for v in result.values()):
            raise ValueError('Scraping through Aylien API yielded no results.')

        result['date'] = datetime.strptime(extract['publishDate'][:10], "%Y-%m-%d").date()
        return result
