from django.contrib import admin
from .models.article import Article
from .models.source import Source
from .models.publication import Publication


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'date', 'summary', 'featured', 'get_image')


admin.site.register(Article, ArticleAdmin)

admin.site.register(Source)
admin.site.register(Publication)
