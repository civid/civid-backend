from django.conf.urls import url
from .views.article import ArticleView, ArticleDetail, ArticleSubmitUrlView
from .views.source import SourceView
from .views.publication import PublicationView


urlpatterns = [

    # Articles
    url(r'^articles$', ArticleView.as_view()),
    url(r'^articles/submit_url$', ArticleSubmitUrlView.as_view()),
    url(r'^articles/(?P<article_id>[\d]+)$', ArticleDetail.as_view()),

    # Sources
    url(r'^sources$', SourceView.as_view()),
    url(r'^publications$', PublicationView.as_view()),
]
