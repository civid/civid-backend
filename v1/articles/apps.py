from django.apps import AppConfig


class ArticlesConfig(AppConfig):
    name = 'v1.articles'
