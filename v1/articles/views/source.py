from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from v1.articles.models.source import Source
from v1.articles.serializers.source import SourceSerializer, SourceSerializerCreate
from v1.filters.common import filter_query_params


# sources
class SourceView(APIView):

    @staticmethod
    def get(request):
        """List sources."""
        sources = filter_query_params({'article': lambda x: int(x)}, Source.objects.all(), request)
        if type(sources) == Response:
            return sources
        return Response(SourceSerializer(sources, many=True).data)

    @staticmethod
    def post(request):
        """Create source."""
        serializer = SourceSerializerCreate(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(SourceSerializer(serializer.instance).data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
