from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from v1.articles.models.publication import Publication
from v1.articles.serializers.publication import PublicationSerializer, PublicationSerializerCreate, PublicationSerializerUpdate


# publications
class PublicationView(APIView):

    @staticmethod
    def get(request):
        """List publications."""
        publications = Publication.objects.all()
        if type(publications) == Response:
            return publications
        return Response(PublicationSerializer(publications, many=True).data)

    @staticmethod
    def post(request):
        """Create publication."""
        serializer = PublicationSerializerCreate(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(SourceSerializer(serializer.instance).data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

