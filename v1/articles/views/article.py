from django.shortcuts import get_object_or_404
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.db import transaction
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import date

from v1.articles.models.article import Article
from v1.articles.models.source import Source
from v1.articles.models.publication import Publication
from v1.articles.serializers.article import (
    ArticleSerializer, ArticleSerializerCreate, ArticleSerializerUpdate, ArticleSerializerFull
)
from v1.utils import constants
#from v1.articles.scrapers import MockArticleScraper as Scraper
from v1.articles.scrapers import AylienScraper as Scraper


feature_param_mapping = {
    'true': True,
    'false': False,
}


# articles
class ArticleView(APIView):

    @staticmethod
    def get(request):
        """List articles."""
        articles = Article.objects.all()
        featured_param = 'featured'
        if featured_param in request.query_params:
            try:
                value = feature_param_mapping[request.query_params[featured_param]]
            except KeyError:
                value = None
            if value is not None:
                articles = articles.filter(featured=value)

        return Response(ArticleSerializer(articles, many=True).data)

    @staticmethod
    def post(request):
        """Create article."""
        serializer = ArticleSerializerCreate(
            data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(ArticleSerializer(serializer.instance).data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# articles/{article_id}
class ArticleDetail(APIView):

    @staticmethod
    def get(request, article_id):
        """View individual article."""
        article = get_object_or_404(Article, pk=article_id)
        return Response(ArticleSerializerFull(article).data)

    @staticmethod
    def patch(request, article_id):
        """Update article."""
        article = get_object_or_404(Article, pk=article_id)
        serializer = ArticleSerializerUpdate(article, data=request.data, context={
                                             'request': request}, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(ArticleSerializerFull(serializer.instance).data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def delete(request, article_id):
        """Delete article."""
        article = get_object_or_404(Article, pk=article_id)
        article.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# articles/submit_url
class ArticleSubmitUrlView(APIView):
    @staticmethod
    def post(request):
        """Create article based on URL."""
        # Check if parameters are valid first
        validator = URLValidator(message='Enter a valid URL.')
        bad_request = 'url' not in request.data
        error = None
        url_value = None if bad_request else request.data['url']
        if bad_request:
            error = 'URL was not specified.'
        else:
            try:
                validator(url_value)
            except ValidationError as e:
                bad_request = True
                error = e.message
        if bad_request:
            return Response({constants.ERROR: error}, status=status.HTTP_400_BAD_REQUEST)

        # Use scraper API to fetch data about URL
        scraper = Scraper()
        try:
            url_data = scraper.fetch_url_data(url_value)
        except ValueError:
            return Response({constants.ERROR: 'URL provided cannot be processed.'}, status=status.HTTP_400_BAD_REQUEST)

        # Find matching publication item
        publication_query = Publication.objects.filter(
            url_substring__isnull=False).values_list('id', 'url_substring')
        try:
            publication_id = next(pub_id for (
                pub_id, url_substring) in publication_query if url_substring in url_value)
        except StopIteration:
            publication_id = Publication.objects.get(
                url_substring__isnull=True).id

        # Create new article & linked items
        with transaction.atomic():
            title = url_data['title']
            summary = url_data['summary'][:500]
            date = url_data['date']
            article = Article.objects.create(
                title=title, image_url=url_data['image'], date=date, summary=summary)
            Source.objects.create(
                title=title, url=url_value, date=date, pub_id=publication_id, article=article)

        return Response(ArticleSerializer(article).data, status=status.HTTP_201_CREATED)
