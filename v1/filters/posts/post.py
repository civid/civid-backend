from rest_framework import status
from rest_framework.response import Response
from datetime import datetime, timezone, timedelta

from v1.utils import constants
from v1.filters.common import filter_query_params


allowed = {
    'user': lambda x: int(x),
    'article': lambda x: int(x),
}


def post_filter(request, query):
    """Filter results based on request query parameters."""
    filtered = filter_query_params(allowed, query, request)
    filter_kwargs = {}
    # Post age param
    post_age_days_param = 'post_age_days'
    if post_age_days_param in request.query_params:
        try:
            post_age_days = int(request.query_params[post_age_days_param])
        except ValueError as e:
            return Response({constants.ERROR: {post_age_days_param: str(e)}}, status=status.HTTP_400_BAD_REQUEST)
        created_date_threshold = datetime.now(timezone.utc) - timedelta(days=post_age_days)
        filter_kwargs['created_date__gte'] = created_date_threshold
    # Following param
    following_param = 'following'
    if following_param in request.query_params and request.query_params[following_param] == 'true':
        if not request.user.is_authenticated:
            return Response(
                {constants.ERROR: 'Following filtering is available only for signed in users.'},
                status=status.HTTP_400_BAD_REQUEST)
        filter_kwargs['user__profile__followed_by'] = request.user.profile
    return filtered.filter(**filter_kwargs) if len(filter_kwargs) > 0 else filtered
