from django.shortcuts import get_object_or_404
from django.db.models import Count
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.pagination import PageNumberPagination

from v1.filters.posts.post import post_filter
from v1.posts.models.post import Post
from v1.queue.models.queue import Queue
from v1.posts.serializers.post import (PostSerializer,
                                       PostSerializerCreate,
                                       PostSerializerFull,
                                       PostSerializerUpdate)
from v1.utils import constants


class PostsPageNumberPagination(PageNumberPagination):
    page_size = 9


# posts
class PostView(APIView):

    @staticmethod
    def get(request):
        """List posts."""
        posts = Post.objects.all()
        filtered_posts = post_filter(request, posts)
        # This means some error occurred.
        if type(filtered_posts) == Response:
            return filtered_posts

        sort_param = 'sort'
        if sort_param in request.query_params:
            sort_type = request.query_params[sort_param]
            if sort_type == 'comments':
                sorted_posts = filtered_posts.annotate(
                    total_replies=Count('post_replies')).exclude(total_replies__lte=2).order_by('-total_replies', 'id')
                sorted_posts = sorted_posts.filter(review_status='3')
            elif sort_type == 'random':
                sorted_posts = filtered_posts.annotate(
                    total_replies=Count('post_replies'))
                sorted_posts = sorted_posts.filter(review_status='3')
            else:
                return Response(
                    {constants.ERROR: f"Sorting type '{sort_type}' is not valid."},
                    status=status.HTTP_400_BAD_REQUEST)
        else:
            sorted_posts = filtered_posts.annotate(
                    total_replies=Count('post_replies')).exclude(total_replies__gte=3).order_by('-pub_date', 'id')
            sorted_posts = sorted_posts.filter(review_status='3')
        paginator = PostsPageNumberPagination()
        page = paginator.paginate_queryset(sorted_posts, request)
        if page is not None:
            serializer = PostSerializer(page, many=True)
            return paginator.get_paginated_response(serializer.data)

        return Response(PostSerializer(sorted_posts, many=True).data)

    @staticmethod
    def post(request):
        """Create a post."""
        serializer = PostSerializerCreate(
            data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(PostSerializer(serializer.instance).data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# posts/{post_slug}
class PostDetail(APIView):

    @staticmethod
    def get(request, post_slug):
        """View an individual post."""
        post = get_object_or_404(Post, slug=post_slug)
        return Response(PostSerializerFull(post).data)

    @staticmethod
    def patch(request, post_slug):
        """Update a post."""
        post = get_object_or_404(Post, slug=post_slug)
        serializer = PostSerializerUpdate(post, data=request.data, context={
                                          'request': request}, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(PostSerializerFull(serializer.instance).data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def delete(request, post_slug):
        """Delete a post."""
        post = get_object_or_404(Post, slug=post_slug)
        if post.user != request.user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        post.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class PostQueueView(APIView):

    @staticmethod
    def get(request):
        """List posts from queue."""
        post_in_queue = Queue.objects.order_by('order').first()

        if post_in_queue is None:
            return Response({constants.ERROR: "Queue is empty!"}, status=status.HTTP_400_BAD_REQUEST)

        return Response(PostSerializerFull(post_in_queue.post).data)

class FeaturedPostView(APIView):

    @staticmethod
    def get(request):
        """List posts."""
        posts = Post.objects.all()
        filtered_posts = post_filter(request, posts)
        # This means some error occurred.
        if type(filtered_posts) == Response:
            return filtered_posts

        sorted_posts = filtered_posts.get(review_status='5')
        return Response(PostSerializerFull(sorted_posts).data)

class PreviousPostView(APIView):

    @staticmethod
    def get(request):
        """List posts."""
        posts = Post.objects.all()
        filtered_posts = post_filter(request, posts)
        # This means some error occurred.
        if type(filtered_posts) == Response:
            return filtered_posts

        sorted_posts = filtered_posts.order_by('-pub_date').filter(review_status='3')

        return Response(PostSerializerFull(sorted_posts[1:4], many=True).data)
