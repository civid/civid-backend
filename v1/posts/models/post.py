import bleach
from enum import Enum
from functools import reduce
from django.conf import settings
from django.db import models
from datetime import datetime, timezone, timedelta
from django.utils.text import slugify
from django.utils import timezone
from v1.general.created_modified import CreatedModified
from v1.general.responsive_image import ResponsiveImage
# todo after merge and run migration can safely be removed
from v1.articles.models.article import Article
from django.contrib.postgres.fields import JSONField
# end todo
from ...utils.constants import review_status
from django.contrib.contenttypes.fields import GenericRelation
from v1.votes.models.vote import Vote


ALLOWED_TAGS = [
    'p',
    'strong',
    'u',
    'em',
    'ul',
    'li',
    'ol',
    'a',
    'blockquote',
    'br',
    'h1',
    'h2',
    'h3',
    'h4',


]

ALLOWED_ATTRIBUTES = {
    'a': ['href', 'title']
}

ALLOWED_PROTOCOLS = [
    ['http', 'https']
]


class PostStatus(Enum):
    NEW = 'new'
    AWARD_PENDING = 'award_pending'
    AWARD_EXPIRED = 'award_expired'
    CLOSED = 'closed'


post_lifespans_to_statuses = [
    (timedelta(seconds=3600), PostStatus.NEW),
    (timedelta(days=1000), PostStatus.AWARD_PENDING),
    (timedelta(days=1000), PostStatus.AWARD_EXPIRED),
]


def bleach_clean(text):
    return bleach.clean(text,
                        tags=ALLOWED_TAGS,
                        protocols=ALLOWED_PROTOCOLS,
                        attributes=ALLOWED_ATTRIBUTES,
                        strip_comments=True,
                        # strip=True
                        )


def default_json():
    # todo after merge and run migration can safely be removed
    return dict()


class Post(CreatedModified, ResponsiveImage):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    title = models.CharField(max_length=160)

    claim_0 = models.CharField(max_length=2000, blank=True)
    claim_1 = models.CharField(max_length=2000, blank=True)
    claim_2 = models.CharField(max_length=2000, blank=True)
    claim_3 = models.CharField(max_length=2000, blank=True)
    votes = GenericRelation(Vote)

    review_status = models.CharField(max_length=1,
                                     choices=review_status,
                                     default="3"
                                     )

    anonymous = models.BooleanField(default=False)

    # not in use
    #article = models.ForeignKey(Article,
    #                            on_delete=models.CASCADE,
    #                            blank=True,
    #                            null=True
    #                            )


    slug = models.SlugField(max_length=140, unique=True, blank=True)
    pub_date = models.DateTimeField(blank=True, null=True, default=timezone.now)

    def __str__(self):
        return self.title

    def _get_unique_slug(self):
        slug = slugify(self.title)
        unique_slug = slug
        num = 1
        while Post.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, num)
            num += 1
        return unique_slug

    def direct_replies(self):
        return self.post_replies.filter(parent=None)

    def status(self):
        if self.pub_date:
            time_elapsed = datetime.now(timezone.utc) - self.pub_date
        else:
            time_elapsed = datetime.now(timezone.utc) - self.created_date
        for (lifespan, status) in post_lifespans_to_statuses:
            time_elapsed -= lifespan
            if time_elapsed.days < 0 and self.review_status == '3' or self.review_status == '5':
                return status
        return PostStatus.CLOSED

    def trophy_award_deadline(self):
        total_lifespan = reduce(
            lambda x, y: x + y, [l[0] for l in post_lifespans_to_statuses[:2]])
        return self.pub_date + total_lifespan

    def title_claims(self):
        html = f"Title:<p> {self.title}</p>"
        html += f"Claim:<p> {self.claim_0}</p>"
        if len(self.claim_1) > 0:
            html += f"Claim:<p> {self.claim_1}</p>"
        if len(self.claim_2) > 0:
            html += f"Claim:<p> {self.claim_2}</p>"
        if len(self.claim_3) > 0:
            html += f"Claim:<p> {self.claim_3}</p>"

        return html
    title_claims.allow_tags = True
    title_claims.short_description = "Post"

    def save(self, *args, **kwarg):
        self.title = bleach_clean(self.title)
        self.claim_0 = bleach_clean(self.claim_0)
        self.claim_1 = bleach_clean(self.claim_1)
        self.claim_2 = bleach_clean(self.claim_2)
        self.claim_3 = bleach_clean(self.claim_3)
        if not self.slug:
            self.slug = self._get_unique_slug()
        super(Post, self).save()

    def get_sizes(self):
        return (320, 480, 640, 769,  900)

    def thumbnail(self):
        return (50,)

    class Meta:
        default_related_name = 'posts'
