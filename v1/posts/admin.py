from django.db.models import F
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import admin
from django.contrib.messages import constants as messages
from .models.post import Post
from ..queue.models.queue import Queue
from ..utils.constants import (ENQUEUE,
                               PENDING,
                               REJECTED,
                               QUEUE_GAP)


class PostAdmin(admin.ModelAdmin):
    list_display = ('user', 'title_claims',
                    'get_image', 'review_status', 'anonymous',
                    'created_date', 'modified_date', 'pub_date')

    list_filter = ('review_status',)

    actions = ['send_to_queue',
               'remove_from_queue',
               'rejected_to_pending',
               'pending_to_rejected'
               ]

    ordering = ("-created_date",)

    prepopulated_fields = {'slug': ('title',)}

    def send_to_queue(self, request, queryset):

        number_of_updates = queryset.filter(review_status=PENDING).count()

        if number_of_updates > 0:

            try:
                order = Queue.objects.all().latest('order').order + QUEUE_GAP
            except ObjectDoesNotExist:
                order = QUEUE_GAP

            for post_instance in queryset.filter(review_status=PENDING):
                post_instance.review_status = ENQUEUE
                post_instance.save()

                if not Queue.objects.filter(post=post_instance):
                    queue = Queue()
                    queue.post = post_instance
                    queue.order = order
                    queue.save()
                    order += QUEUE_GAP

        if number_of_updates == 0:
            self.message_user(request,
                              'No post was sent to queue.',
                              level=messages.WARNING)
        else:
            message_part = 'One post was' if number_of_updates == 1 else f'{number_of_updates} posts were'
            self.message_user(request, f"{message_part} sent to queue.")

    send_to_queue.short_description = "Send to queue"

    def remove_from_queue(self, request, queryset):
        enqueue = Queue.objects.all().order_by('order')
        number_of_updates = queryset.filter(review_status=ENQUEUE).count()

        if number_of_updates > 0:
            for post in queryset.filter(review_status=ENQUEUE):
                post.review_status = PENDING
                post.save()

                queued_post = Queue.objects.filter(post=post)
                if queued_post:
                    order = queued_post[0].order
                    Queue.objects.filter(order__gt=order).order_by(
                        'order').update(order=F('order') - QUEUE_GAP)

                    queued_post.delete()

        if number_of_updates == 0:
            self.message_user(request, 'No post was removed from queue.')
        else:
            message_part = 'One post was' if number_of_updates == 1 else f'{number_of_updates} posts were'
            self.message_user(request, f"{message_part} removed from queue.")

    remove_from_queue.short_description = "Remove from queue"

    def rejected_to_pending(self, request, queryset):
        number_of_updates = 0
        for q in queryset:
            if q.review_status == REJECTED:
                q.review_status = PENDING
                q.save()
                number_of_updates += 1

        if number_of_updates == 0:
            self.message_user(
                request, 'No post changed status from reject to pending.')
        else:
            message_part = 'One post' if number_of_updates == 1 else f'{number_of_updates} posts'
            self.message_user(
                request, f"{message_part} changed status from rejected to pending.")

    rejected_to_pending.short_description = "Change status from Reject to Pending"

    def pending_to_rejected(self, request, queryset):
        number_of_updates = 0
        for q in queryset:
            if q.review_status == PENDING:
                q.review_status = REJECTED
                q.save()
                number_of_updates += 1

        if number_of_updates == 0:
            self.message_user(
                request, 'No post changed status from pending to rejected.')
        else:
            message_part = 'One post' if number_of_updates == 1 else f'{number_of_updates} posts'
            self.message_user(
                request, f"{message_part} changed status from pending to rejected.")

    pending_to_rejected.short_description = "Change status from Pending to Rejected"

    # todo refactor this
    # def get_actions(self, request):
    #     """ disable delete_selected for this model"""
    #     actions = super().get_actions(request)
    #     if 'delete_selected' in actions:
    #         del actions['delete_selected']
    #     return actions


admin.site.register(Post, PostAdmin)
