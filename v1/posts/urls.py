from django.conf.urls import url
from .views.post import (PostView,
                         FeaturedPostView,
                         PreviousPostView,
                         PostDetail,
                         PostQueueView)


urlpatterns = [

    # Posts
    url(r'^posts$', PostView.as_view()),
    url(r'^post-in-queue$', PostQueueView.as_view()),
    url(r'^featured-post$', FeaturedPostView.as_view()),
    url(r'^previous-posts$', PreviousPostView.as_view()),
    url(r'^posts/(?P<post_slug>[\w-]+)/$', PostDetail.as_view()),

]
