# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2019-03-26 09:52
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, upload_to='')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(max_length=160)),
                ('claim_0', models.CharField(blank=True, max_length=2000)),
                ('claim_1', models.CharField(blank=True, max_length=2000)),
                ('claim_2', models.CharField(blank=True, max_length=2000)),
                ('claim_3', models.CharField(blank=True, max_length=2000)),
                ('review_status', models.CharField(choices=[('1', 'Pending'), ('2', 'Rejected'), ('3', 'Published'), ('4', 'Sent to queue'), ('5', 'Featured')], default='3', max_length=1)),
                ('anonymous', models.BooleanField(default=False)),
                ('slug', models.SlugField(blank=True, max_length=140, unique=True)),
                ('pub_date', models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='posts', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'default_related_name': 'posts',
            },
        ),
    ]
