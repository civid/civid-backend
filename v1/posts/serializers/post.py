from rest_framework import serializers
from v1.accounts.serializers.user import UserSerializer
from v1.posts.models.post import Post
from v1.replies.models.post_reply import PostReply
from v1.replies.serializers.post_reply import PostReplySerializer
from v1.votes.serializers.vote import VoteSerializer


class PostSerializer(serializers.ModelSerializer):
    post_reply_count = serializers.SerializerMethodField()
    images_sizes = serializers.SerializerMethodField()
    thumbnail_size = serializers.SerializerMethodField()
    username = serializers.SerializerMethodField()
    influence = serializers.SerializerMethodField()
    votes = VoteSerializer(many=True, read_only=True)
    user = UserSerializer()

    class Meta:
        model = Post
        fields = '__all__'

    @staticmethod
    def get_post_reply_count(post):
        return PostReply.objects.filter(post=post).count()

    @staticmethod
    def get_images_sizes(obj):
        return obj.get_sizes()

    @staticmethod
    def get_thumbnail_size(obj):
        return obj.thumbnail()

    @staticmethod
    def get_username(obj):
        return obj.user.get_user_name()

    @staticmethod
    def get_influence(obj):
        return obj.user.influence


class PostSerializerCreate(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Post
        fields = '__all__'

    def validate(self, data):

        # todo remove this when we are sure that aricles are not returning to site
        # article = 'article' in self.initial_data and self.initial_data['article'] or None
        # num_posts = Post.objects.filter(
        #     article=article, user=self.context['request'].user).count()

        # if num_posts > 0:
        #     raise serializers.ValidationError(
        #         'You already have an active discussion relating to this topic. Award a user a trophy to be able to create a new one.')
        return data


class PostSerializerFull(PostSerializer):
    direct_replies = PostReplySerializer(many=True, read_only=True)
    status = serializers.SerializerMethodField()
    trophy_award_deadline = serializers.SerializerMethodField()

    @staticmethod
    def get_status(post):
        return post.status().value

    @staticmethod
    def get_images_sizes(obj):
        return obj.get_sizes()

    @staticmethod
    def get_trophy_award_deadline(post):
        return post.trophy_award_deadline()


class PostSerializerUpdate(serializers.ModelSerializer):

    class Meta:
        model = Post
        exclude = ('user',)

    def validate(self, data):
        """Validate authenticated user."""
        if self.instance.user != self.context['request'].user:
            raise serializers.ValidationError(
                'You can not edit posts from other users')
        return data
