from django.db import models


class Feedback(models.Model):

    message = models.CharField(max_length=64000)
    user = models.CharField(max_length=20, blank=True)

    def __str__(self):
        return self.user + ": " + self.message
