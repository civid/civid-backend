from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from v1.feedback.models.feedback import Feedback
from v1.feedback.serializers.feedback import (FeedbackSerializer,
                                             FeedbackSerializerCreate)

# Feedback


class FeedbackView(APIView):


    @staticmethod
    def get(request):
        feedback = Feedback.objects.all()
        return Response(FeedbackSerializer(feedback, many=True).data)

    @staticmethod
    def post(request):
        """Create Feedback."""
        serializer = FeedbackSerializerCreate(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(FeedbackSerializer(serializer.instance).data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# feedback/{feedback_id}
class FeedbackDetail(APIView):

    @staticmethod
    def get(request, Feedback_id):
        Feedback = get_object_or_404(Feedback, pk=Feedback_id)
        return Response(FeedbackSerializer(Feedback).data)
