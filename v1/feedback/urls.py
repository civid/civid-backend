from django.conf.urls import url
from .views.feedback import FeedbackView, FeedbackDetail


urlpatterns = [

    # Feedback
    url(r'^feedback$', FeedbackView.as_view()),
    url(r'^feedback/(?P<feedback_id>[\d]+)$',
        FeedbackDetail.as_view()
        ),

]
