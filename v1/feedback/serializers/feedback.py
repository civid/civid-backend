from rest_framework import serializers

from v1.utils import constants
from v1.utils.permissions import is_administrator
from v1.feedback.models.feedback import Feedback



class FeedbackSerializer(serializers.ModelSerializer):

    class Meta:
        model = Feedback
        fields = '__all__'


class FeedbackSerializerCreate(serializers.ModelSerializer):

    class Meta:
        model = Feedback
        fields = '__all__'

