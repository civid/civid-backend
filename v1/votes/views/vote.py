from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from v1.votes.models.vote import Vote
from v1.posts.models.post import Post
from v1.replies.models.post_reply import PostReply
from v1.votes.serializers.vote import (VoteSerializer,
                                             VoteSerializerCreate,
                                             VoteSerializerUpdate)
from django.contrib.contenttypes.models import ContentType
from v1.utils import constants

# votes


class VoteView(APIView):

    @staticmethod
    def post(request):
        """
        Create vote
        """
        if 'recipient' not in request.data:
            return Response({constants.ERROR: 'No user specified to upvote.'},
                            status=status.HTTP_400_BAD_REQUEST
                            )
        user = get_object_or_404(
            get_user_model(), pk=request.data.get('recipient')
        )
        if hasattr(request.data, '_mutable'):
            request.data._mutable = True

        if request.data['type_name'] == 'P':
             ct = ContentType.objects.get_for_model(Post)
        elif request.data['type_name'] == 'R':
            ct = ContentType.objects.get_for_model(PostReply)

        request.data['content_type'] = ct.id
        request.data['object_id'] = request.data['content_object']['id']


        user.influence += request.data['value']
        user.save()

        serializer = VoteSerializerCreate(
            data=request.data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response(VoteSerializer(serializer.instance).data, status=status.HTTP_201_CREATED)
        print(serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# votes/{vote_id}
class VoteDetail(APIView):
    @staticmethod
    def get(request, vote_id):
        """View an individual vote."""
        vote = get_object_or_404(Vote, pk=vote_id)
        return Response(VoteSerializer(vote).data)

    @staticmethod
    def patch(request, vote_id):
        """
        Update vote
        """

        vote = get_object_or_404(Vote, pk=vote_id)
        user = get_object_or_404(get_user_model(), pk=vote.recipient.id)
        user.influence -= vote.value
        user.influence += request.data['value']
        user.save()
        serializer = VoteSerializerUpdate(vote, data=request.data, context={
                                               'request': request}, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(VoteSerializer(serializer.instance).data)
        print(serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def delete(request, vote_id):
        """
        Delete vote
        """

        vote = get_object_or_404(Vote, pk=vote_id)
        user = get_object_or_404(get_user_model(), pk=vote.recipient.id)
        user.influence -= vote.value
        user.save()

        if vote.user != request.user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        vote.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
