from rest_framework import serializers
from v1.votes.models.vote import Vote
from v1.accounts.serializers.user import UserSerializer


class VoteSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Vote
        fields = '__all__'


class VoteSerializerCreate(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Vote
        fields = '__all__'


class VoteSerializerUpdate(serializers.ModelSerializer):

    class Meta:
        model = Vote
        exclude = ('user',)

    def validate(self, data):
        """Validate authenticated user"""
        if self.instance.user != self.context['request'].user:
            raise serializers.ValidationError(
                'You can not edit reply votes from other users')
        return data
