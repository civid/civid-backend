from django.conf.urls import url
from .views.vote import VoteView, VoteDetail


urlpatterns = [

    # Post votes
    url(r'^votes$', VoteView.as_view()),
    url(r'^votes/(?P<vote_id>[\d]+)$',
        VoteDetail.as_view()
        ),

]
