from django.conf import settings
from django.db import models
from v1.utils import constants
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class Vote(models.Model):

    POST_TYPES = (
        ('P', 'Post'),
        ('R', 'Reply'),
    )

    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="upvotes_awarded")
    recipient = models.ForeignKey(settings.AUTH_USER_MODEL)
    created_date = models.DateTimeField(auto_now_add=True)
    value = models.IntegerField(choices=constants.VOTE_VALUE_CHOICES)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    type_name = models.CharField(max_length=1, choices=POST_TYPES)

    class Meta:
        default_related_name = 'votes'
        unique_together = (('user', 'content_type', 'object_id'))
