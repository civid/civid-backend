from django.db import models
from ..utils.responsive_image import (save_responsive_images,
                                      make_file_name)


class ResponsiveImage(models.Model):

    image = models.ImageField(blank=True)

    def save(self, *args, **kwargs):
        super(ResponsiveImage, self).save(*args, **kwargs)
        # create responsive images
       # if self.image and hasattr(self.image, 'url'):
       #     save_responsive_images(self.image.file.name, self.get_sizes())

    # todo remove images when deleteing record or clear the image
    # do it with signals

    def get_image(self):
        if self.image and hasattr(self.image, 'url'):
            #return f'<img src="{self.image.url.replace(".","-320w.")}"/>'
            return f'<img src="{self.image.url}"/>'
        return "No image set."
    get_image.allow_tags = True
    get_image.short_description = "Image 320 wide"

    def get_sizes(self):
        """ Override this method to return other sizes"""
        return [
            320,
            480,
            640,
            800,
            900,
        ]

    def get_thumbnail(self):
        return 50

    def get_default(self):
        """ Override this methos to return other default"""
        # the third in the list
        return 3

    class Meta:
        abstract = True
