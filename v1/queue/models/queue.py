from datetime import date, timedelta
from django.db import models
from ...posts.models.post import Post
from v1.general.created_modified import CreatedModified


class Queue(CreatedModified):
    """ Queue user opinions to be published """

    post = models.ForeignKey(Post, related_name="posts_in_queue")
    order = models.IntegerField(default=0, db_index=True)

    def __str__(self):
        return f"{self.post.title}"

    def get_title(self):
        return self.post.title_claims()
    get_title.allow_tags = True
    get_title.short_description = "Post"

    def get_image(self):
        return self.post.get_image()
    get_image.allow_tags = True
    get_image.short_description = "Image"

    def publication_date(self):
        today = date.today()
        """ potential to slow down things."""
        for count, post in enumerate(Queue.objects.all().order_by('order')):
            if post.post.id == self.post.id:
                return today + timedelta(days=count), ' 12:00 p.m.'

    publication_date.allow_tags = True
    publication_date.short_description = "Publishing"

    class Meta:
        verbose_name_plural = "Posts in queue"
