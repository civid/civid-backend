from django.db.models import F
from django.contrib.messages import constants as messages
from django.contrib import admin
from .models.queue import Queue
from ..posts.models.post import Post
from ..utils.constants import (REJECTED,
                               QUEUE_GAP)


class QueueAdmin(admin.ModelAdmin):
    list_display = ('get_title', 'order', 'get_image',
                    'created_date', 'publication_date')

    actions = ('reject',
               'remove_from_queue',
               'swap_positions',
               'post_renumber')

    list_editable = ('order',)
    save_on_top = True
    ordering = ('order', 'created_date')

    def reject(self, request, queryset):

        for q in queryset:
            Post.objects.filter(id=q.post.id).update(review_status=REJECTED)
            q.delete()

        start = QUEUE_GAP
        for q in Queue.objects.all():
            q.order = start
            q.save()
            start += QUEUE_GAP

        self.message_user(request,
                          f'{queryset.count()} posts removed from queue.')

    reject.short_description = "Remove from queue and change Post status to rejected."

    def remove_from_queue(self, request, queryset):

        for q in queryset:
            q.delete()

        start = QUEUE_GAP
        for q in Queue.objects.all():
            q.order = start
            q.save()
            start += QUEUE_GAP

        self.message_user(request,
                          f'{queryset.count()} posts removed from queue.')

    remove_from_queue.short_description = "Remove from queue."

    def swap_positions(self, request, queryset):
        if queryset.count() != 2:
            self.message_user(request,
                              'Need to selected two post.',
                              level=messages.WARNING)
        else:
            post0, post1 = queryset[0], queryset[1]
            post0.order, post1.order = post1.order, post0.order
            post0.save()
            post1.save()
            self.message_user(request, 'Two posts swapped order.')

    swap_positions.short_description = "Swap posts positions"

    def post_renumber(self, request, queryset):

        start = QUEUE_GAP
        for q in queryset:
            q.order = start
            q.save()
            start += QUEUE_GAP

        self.message_user(request, f'{queryset.count()} posts renumbered.')

    post_renumber.short_description = "Renumber selected posts"

    # todo refactor this
    def get_actions(self, request):
        """disable delete_selected for this model"""
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions


admin.site.register(Queue, QueueAdmin)
