from django.conf.urls import url

from .views import NotificationsListView, MarkNotificationsAsReadView


urlpatterns = [
    url(r'^notifications$', NotificationsListView.as_view()),
    url(r'^notifications/mark-as-read$', MarkNotificationsAsReadView.as_view()),
]
