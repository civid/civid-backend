from rest_framework import serializers

from v1.accounts.models.user import User
from v1.accounts.serializers.user import UserSerializer
from v1.replies.models.post_reply import PostReply
from v1.replies.serializers.post_reply import PostReplySerializer
from v1.articles.models.article import Article
from v1.articles.serializers.article import ArticleSerializer
from v1.trophies.models.trophy import Trophy
from v1.trophies.serializers.trophy import TrophySerializer
from v1.posts.models.post import Post
from v1.posts.serializers.post import PostSerializerFull


class GenericNotificationRelatedField(serializers.RelatedField):
    def to_representation(self, value):
        serializer = None
        if isinstance(value, User):
            serializer = UserSerializer(value)
        if isinstance(value, PostReply):
            serializer = PostReplySerializer(value)
        if isinstance(value, Article):
            serializer = ArticleSerializer(value)
        if isinstance(value, Trophy):
            serializer = TrophySerializer(value)
        if isinstance(value, Post):
            serializer = PostSerializerFull(value)
        return serializer.data if serializer else None


class NotificationSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    actor = GenericNotificationRelatedField(read_only=True)
    recipient = UserSerializer(read_only=True)
    verb = serializers.CharField(read_only=True)
    level = serializers.CharField(read_only=True)
    target = GenericNotificationRelatedField(read_only=True)
    action_object = GenericNotificationRelatedField(read_only=True)
    timestamp = serializers.DateTimeField(read_only=True)


class NotificationIdsSerializer(serializers.Serializer):
    ids = serializers.ListField(child=serializers.IntegerField(), min_length=1)
