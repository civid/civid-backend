from django.apps import AppConfig


class NotifyConfig(AppConfig):
    name = 'v1.notify'

    def ready(self):
        # noinspection PyUnresolvedReferences
        import v1.notify.signals
