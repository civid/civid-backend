from django.db.models.signals import post_save, m2m_changed
from django.dispatch import receiver
from notifications.signals import notify

from v1.replies.models.post_reply import PostReply
from v1.accounts.models.profile import Profile
from v1.trophies.models.trophy import AwardedTrophy
from v1.posts.models.post import Post


@receiver(post_save, sender=PostReply)
def notify_about_reply_to_comment(sender, **kwargs):
    reply, created = [kwargs[key] for key in ('instance', 'created')]
    # Notify only about:
    # - new replies
    # - indirect replies to posts
    # - from another user
    if not created or not reply.parent or reply.user == reply.parent.user:
        return
    notify.send(reply.user, recipient=reply.parent.user, verb='REPLY_TO_COMMENT', action_object=reply,
                target=reply.post)


@receiver(post_save, sender=PostReply)
def notify_about_response_to_opinion(sender, **kwargs):
    reply, created = [kwargs[key] for key in ('instance', 'created')]
    # Notify only about:
    # - new replies
    # - direct replies to posts
    # - from another user
    if not created or reply.parent or reply.user == reply.post.user:
        return
    notify.send(reply.user, recipient=reply.post.user, verb='RESPONSE_TO_OPINION', action_object=reply,
                target=reply.post, level='success')


@receiver(m2m_changed, sender=Profile.follows.through)
def notify_about_new_follower(sender, **kwargs):
    follower_profile, action, pk_set = [kwargs[key] for key in ('instance', 'action', 'pk_set')]
    # Notify only about:
    # - new followers
    # - consider only one follower at a time, multiple is likely to be some kind of unrelated batch update
    if action != 'post_add' or len(pk_set) > 1:
        return
    followee = Profile.objects.get(pk=next(iter(pk_set))).user
    notify.send(follower_profile.user, recipient=followee, verb='NEW_FOLLOWER', level='success')


@receiver(post_save, sender=AwardedTrophy)
def notify_about_received_trophy(sender, **kwargs):
    awarded_trophy, created = [kwargs[key] for key in ('instance', 'created')]
    # Notify only about:
    # - new awards
    if not created:
        return
    if isinstance(awarded_trophy.context, Post):
        notify.send(awarded_trophy.context.user, recipient=awarded_trophy.user, verb='RECEIVED_TROPHY',
                    action_object=awarded_trophy.trophy, level='success')
