from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import NotificationSerializer, NotificationIdsSerializer


class NotificationsListView(APIView):
    """
    List unread notifications for current user
    """
    @staticmethod
    def get(request):
        current_user = request.user
        if current_user.is_authenticated:
            unread_notifications = current_user.notifications.unread()
        else:
            unread_notifications = []
        return Response(NotificationSerializer(unread_notifications, many=True).data)


class MarkNotificationsAsReadView(APIView):
    """
    Mark notifications as read
    """
    @staticmethod
    def post(request):
        serializer = NotificationIdsSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        current_user = request.user
        current_user.notifications.filter(id__in=serializer.validated_data['ids']).mark_all_as_read()
        return Response(None, status=status.HTTP_204_NO_CONTENT)
