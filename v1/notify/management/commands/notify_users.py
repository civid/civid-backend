from django.core.management.base import BaseCommand
from datetime import datetime, timezone, timedelta
from notifications.models import Notification
from django.contrib.contenttypes.models import ContentType
from notifications.signals import notify

from v1.posts.models.post import Post, PostStatus


class Command(BaseCommand):
    help = 'Sends time-based notifications to users'

    def handle(self, *args, **options):
        notifications_sent = 0
        # Trophy award reminder
        for post in (p for p in Post.objects.all() if p.status() == PostStatus.AWARD_PENDING):
            # Check whether is time to send a notification
            #time_elapsed = datetime.now(timezone.utc) - post.created_date
            #if time_elapsed < timedelta(days=2):
            #    continue
            # Check whether notification was already sent
            notification_verb = 'TROPHY_REMINDER'
            user_type = ContentType.objects.get_for_model(post.user)
            post_type = ContentType.objects.get_for_model(post)
            notification_exists = Notification.objects.filter(
                actor_content_type=user_type.id, actor_object_id=post.user.id, recipient=post.user,
                verb=notification_verb, target_content_type=post_type.id, target_object_id=post.id).count() > 0
            if notification_exists:
                continue
            # Send notification
            notify.send(post.user, recipient=post.user, verb=notification_verb, target=post, level='warning')
            notifications_sent += 1
        # Report command results
        self.stdout.write(self.style.SUCCESS(f'Executed successfully, {notifications_sent} notifications sent.'))
