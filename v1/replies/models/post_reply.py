from django.db import models
from v1.posts.models.post import Post
from .reply import Reply


class PostReply(Reply):
    post = models.ForeignKey(Post)
    parent = models.ForeignKey('self',
                               null=True,
                               blank=True,
                               related_name='replies')
    claimparent = models.ForeignKey('self',
                                      null=True,
                                      blank=True,
                                      related_name='all_replies')

    class Meta:
        default_related_name = 'post_replies'
        verbose_name_plural = 'Post replies'
