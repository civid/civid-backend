import bleach
from django.conf import settings
from django.db import models

from v1.general.created_modified import CreatedModified
from django.contrib.contenttypes.fields import GenericRelation
from v1.votes.models.vote import Vote


ALLOWED_TAGS = [
    'p',
    'strong',
    'u',
    'em',
    'ul',
    'li',
    'ol',
    'a',
    'blockquote',
    'br',
    'h1',
    'h2',
    'h3',
    'h4',


]

ALLOWED_ATTRIBUTES = {
    'a': ['href', 'title']
}

ALLOWED_PROTOCOLS = [
    ['http', 'https']
]


def bleach_clean(text):
    return bleach.clean(text,
                        tags=ALLOWED_TAGS,
                        protocols=ALLOWED_PROTOCOLS,
                        attributes=ALLOWED_ATTRIBUTES,
                        strip_comments=True,
                        # strip=True
                        )



class Reply(CreatedModified):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    title = models.CharField(max_length=120)
    claim_0 = models.CharField(max_length=2000, blank=True)
    claim_1 = models.CharField(max_length=2000, blank=True)
    claim_2 = models.CharField(max_length=2000, blank=True)
    claim_3 = models.CharField(max_length=2000, blank=True)
    votes = GenericRelation(Vote)

    class Meta:
        abstract = True

    def title_claims(self):
        html = f"Title:<p> {self.title}</p>"
        html += f"Claim:<p> {self.claim_0}</p>"
        if len(self.claim_1) > 0:
            html += f"Claim:<p> {self.claim_1}</p>"
        if len(self.claim_2) > 0:
            html += f"Claim:<p> {self.claim_2}</p>"
        if len(self.claim_3) > 0:
            html += f"Claim:<p> {self.claim_3}</p>"

        return html
    title_claims.allow_tags = True
    title_claims.short_description = "Post"

    def save(self, *args, **kwarg):
        self.title = bleach_clean(self.title)
        self.claim_0 = bleach_clean(self.claim_0)
        self.claim_1 = bleach_clean(self.claim_1)
        self.claim_2 = bleach_clean(self.claim_2)
        self.claim_3 = bleach_clean(self.claim_3)
        super(Reply, self).save()
