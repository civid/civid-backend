from django.conf.urls import url, include
from .views.post_reply import (PostReplyView,
                               PostReplyDetail,
                               GetPostReply)


urlpatterns = [

    # Post replies
    url(r'^post_replies$', PostReplyView.as_view()),
    url(r'^post_replies/(?P<post_reply_id>[\d]+)$', PostReplyDetail.as_view()),
    url(r'^get_post_replies/(?P<post_id>[\d]+)$', GetPostReply.as_view()),


]
