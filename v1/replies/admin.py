from django.contrib import admin
from .models.post_reply import PostReply


class PostReplyAdmin(admin.ModelAdmin):
    list_display = ('user', 'claim_0', 'claim_1', 'claim_2', 'claim_3', 'title', 'post', 'parent')


admin.site.register(PostReply, PostReplyAdmin)
