from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField
from v1.accounts.serializers.user import UserSerializer
from v1.replies.models.post_reply import PostReply
from v1.votes.serializers.vote import VoteSerializer


class PostReplySerializer(serializers.ModelSerializer):
    user = UserSerializer()
    replies = RecursiveField(many=True)
    votes = VoteSerializer(many=True, read_only=True)

    class Meta:
        model = PostReply
        fields = '__all__'


class PostReplySerializerCreate(serializers.ModelSerializer):

    class Meta:
        model = PostReply
        fields = '__all__'
        extra_kwargs = {
            'title': {'allow_blank': True},
        }

    def validate_user(self, user):
        """
        Validate authenticated user
        """

        if user != self.context['request'].user:
            raise serializers.ValidationError('You can not create post replies for other users')
        return user

    def validate(self, data):
        """
        Validate one parent level comment per discussion
        """

        parent_count = PostReply.objects.filter(post=self.initial_data['post'], parent=None, user=self.context['request'].user).count()
        if parent_count > 0 and 'parent' not in data:
            raise serializers.ValidationError('Sorry, you may only have one parent level response per discussion')
        return data


class PostReplySerializerUpdate(serializers.ModelSerializer):

    class Meta:
        model = PostReply
        exclude = ('post',)
        extra_kwargs = {
            'title': {'allow_blank': True},
        }

    def validate(self, data):
        """
        Validate authenticated user
        """

        if self.instance.user != self.context['request'].user:
            raise serializers.ValidationError('You can not edit post replies from other users')
        return data
