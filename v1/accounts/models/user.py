import uuid
import threading
from datetime import datetime
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from .profile import Profile
from v1.accounts.managers.user_manager import UserManager
from ..views.verification_email import verification_email
from ..views.reset_password_email import reset_password_email
from social_django.models import UserSocialAuth

class User(AbstractBaseUser, PermissionsMixin):

    date_joined = models.DateTimeField(auto_now_add=True)

    username = models.CharField(max_length=20,
                                null=True,
                                blank=False,
                                default='',
                                db_index=True
                                )

    email = models.EmailField(max_length=255,
                              unique=True
                              )

    first_name = models.CharField(max_length=255,
                                  blank=True,
                                  null=True
                                  )

    last_name = models.CharField(max_length=255,
                                 blank=True,
                                 null=True
                                 )

    is_active = models.BooleanField(default=True)

    is_staff = models.BooleanField(default=False)

    influence = models.IntegerField(default=0)

    description = models.CharField(max_length=60,
                                   null=True,
                                   default=None,
                                   blank=True
                                   )

    location = models.CharField(max_length=50,
                                null=True,
                                blank=True
                                )

    work = models.CharField(max_length=50,
                            null=True,
                            blank=True
                            )

    education = models.CharField(max_length=50,
                                 null=True,
                                 blank=True
                                 )

    about_text = models.TextField(max_length=500,
                                  blank=True,
                                  null=True,
                                  default=None
                                  )

    twitter = models.CharField(null=True,
                               blank=True,
                               max_length=15,
                               default=None
                               )

    email_verified = models.BooleanField(default=False)

    verify_key = models.UUIDField(db_index=True,
                                  default=uuid.uuid4,
                                  editable=False
                                  )

    verify_date = models.DateTimeField(auto_now_add=True)

    mailing_opt_in = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    class Meta:
        app_label = 'accounts'

    def __str__(self):
        if self.username == '':
            return self.email

        return self.username

    def get_short_name(self):
        return self.username

    def get_full_name(self):
        return self.username

    def get_user_name(self):
        return self.username

    def get_user_email(self):
        return self.email

    def get_avatar(self):
        return Profile.objects.get(user=self.pk).get_image()

    get_avatar.allow_tags = True
    get_avatar.short_description = "Avatar"

    def get_influence(self):
        return self.influence

    def send_verification_email(self):

        t1 = threading.Thread(target=verification_email,
                              kwargs=dict(user=self)
                              )
        t1.start()
        t1.join()

    def send_password_reset(self):

        t1 = threading.Thread(target=reset_password_email,
                              kwargs=dict(user=self)
                              )
        t1.start()
        t1.join()

    def save(self, *args, **kwargs):
        new_user = self.pk == None

        # if no username, set a default name
        if self.username == self.email or self.username == '':
            new_name = self.email.split("@")[0]
            num = 1
            # concat number to end of username until unique
            while User.objects.filter(username=new_name):
              if num == 1:
                new_name = new_name + str(num)
              else:
                new_name = new_name[:-1] + str(num)
              num += 1
            self.username = new_name

        # set email verified and mailing list
        social = UserSocialAuth.objects.filter(user=self.id)
        if social:
            self.email_verified = True
            self.mailing_opt_in = True

        if not self.password:
            self.password = str(uuid.uuid4()).replace('-', '')
        else:
            # allow change password in admin
            if not self.password.startswith("pbkdf2_sha256$"):
                self.set_password(self.password)

        super(User, self).save(*args, **kwargs)
