from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers
from v1.accounts.models.profile import Profile
from v1.accounts.models.user import User
from v1.credits.models.invitation import Invitation


class AcceptInvitationSerializer(serializers.Serializer):
    email = serializers.EmailField()
    username = serializers.CharField(max_length=100)
    first_name = serializers.CharField(max_length=255)
    last_name = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=128)

    def create(self, validated_data):
        """Create user, set password, update invitation, and create profile."""

        user = User.objects.create_user(
            email=validated_data['email'],
            username=validated_data['user_name'],
            password=validated_data['password'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )
        Profile.objects.create(user=user)
        return user

    def update(self, instance, validated_data):
        pass

    @staticmethod
    def validate_email(value):
        """Check the email is unique."""

        if User.objects.filter(email=value):
            raise serializers.ValidationError('Email already exists')
        return value

    @staticmethod
    def validate_username(value):
        """Check the username is unique."""

        if User.objects.filter(username=value):
            raise serializers.ValidationError('User name already exists')
        return value

    @staticmethod
    def validate_password(password):
        """Validate password."""

        validate_password(password)
        return password
