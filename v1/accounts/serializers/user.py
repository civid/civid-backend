from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ObjectDoesNotExist
import re
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from v1.accounts.models.profile import Profile
from v1.accounts.models.user import User
from v1.utils import constants
from v1.utils.permissions import is_administrator, is_moderator
from .profile import ProfileSerializer


class UserSerializer(serializers.ModelSerializer):
    profile = serializers.SerializerMethodField()
    role = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'email', 'username', 'first_name',
                  'last_name', 'influence', 'description',
                  'location', 'work', 'education', 'about_text',
                  'twitter', 'profile', 'role', 'email_verified',
                  'mailing_opt_in',
                  )

    @staticmethod
    def get_profile(user):
        """Get or create profile."""
        profile, created = Profile.objects.get_or_create(user=user)
        return ProfileSerializer(profile, read_only=True).data

    @staticmethod
    def get_role(user):
        """Get user role."""
        if is_administrator(user):
            return constants.USER_ROLE_ADMINISTRATOR
        if is_moderator(user):
            return constants.USER_ROLE_MODERATOR


class UserSerializerCreate(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('email', 'username', 'first_name', 'last_name', 'password')

    # def validate(self, data):
    #     """Administrator permissions needed."""

    #     if not is_administrator(self.context['request'].user):
    #         raise serializers.ValidationError(constants.PERMISSION_ADMINISTRATOR_REQUIRED)
    #     return data

    @staticmethod
    def validate_password(password):
        """Validate password."""
        validate_password(password)
        return password


    @staticmethod
    def validate_username(value):
        if User.objects.filter(username=value):
            raise serializers.ValidationError('Username already exists.')

        if re.match("^(?=.{3,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$", value) == None:
            raise serializers.ValidationError('Must be between 3-20 characters and use alphanumeric, or ._ characters.')
        return value

    @staticmethod
    def validate_email(value):
        """Check the email is unique."""

        if User.objects.filter(email=value):
            raise serializers.ValidationError('Email already exists.')
        return value


class UserSerializerLogin(UserSerializer):
    token = serializers.SerializerMethodField()

    @staticmethod
    def get_token(user):
        """Get or create token."""
        token, created = Token.objects.get_or_create(user=user)
        return token.key

    class Meta:
        model = User
        fields = ('id', 'email', 'username', 'first_name',
                  'last_name', 'influence', 'description',
                  'location', 'work', 'education', 'about_text',
                  'twitter', 'profile', 'role', 'token',
                  'email_verified', 'mailing_opt_in')


class UserSerializerUpdate(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = '__all__'
