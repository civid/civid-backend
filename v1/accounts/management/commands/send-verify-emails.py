from django.core.management.base import BaseCommand
from django.utils import timezone
from ....accounts.views.verification_email import verification_email
from ....accounts.models.user import User


class Command(BaseCommand):
    help = 'Send Verify emails'

    def handle(self, *args, **kwargs):

        for user in User.objects.filter(email_verified=False,
                                        mailing_opt_in=True):
            verification_email(user=user)
