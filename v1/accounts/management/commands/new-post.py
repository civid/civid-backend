from django.core.management.base import BaseCommand
from v1.posts.models.post import Post
from v1.queue.models.queue import Queue
from django.utils import timezone


class Command(BaseCommand):
    help = 'Publishes next post in queue, remove post from queue'

    def handle(self, *args, **kwargs):
        # Set current featured post to published
        old_post = Post.objects.get(review_status=5)
        old_post.review_status = 3
        old_post.save()

        queue = Queue.objects.order_by('order').first()
        if queue is not None:
            post = Post.objects.get(pk=queue.post_id)
            # Set new post to featured
            post.review_status = 5
            post.pub_date = timezone.now()
            post.save()
            queue.delete()
            print("post status: ", post)
        else:
            print("Error: no posts in queue")
