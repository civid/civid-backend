# todo: with just a couple of users this will do
# when the user base grows a queue should be used to
# send the emails to users

from django.core.management.base import BaseCommand
from ....posts.models.post import Post
from ....accounts.views.mailing_template import mailing_template
from ....accounts.models.user import User


class Command(BaseCommand):
    help = 'Send mailling to users'

    def handle(self, *args, **kwargs):

        new_post = Post.objects.get(review_status=5)

        for user in User.objects.filter(email_verified=True,
                                        mailing_opt_in=True):

            mailing_template(user, new_post)
