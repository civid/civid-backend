from django.core.management.base import BaseCommand
from django.utils import timezone
from ...models.user import User
import uuid


class Command(BaseCommand):
    help = 'Make email verify keys, on migrate all keys are set to same value \n'
    help += 'Should be run only once after migrate !!!'

    def handle(self, *args, **kwargs):

        print("Setting Keys")
        for user in User.objects.all():
            user.verify_key = uuid.uuid4()
            user.save()
