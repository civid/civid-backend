from django.conf import settings
import uuid
import boto3
from botocore.exceptions import ClientError


def reset_password_email(user=''):
    # todo , create a log error

    SENDER = "Civid <noreply@civid.com>"
    AWS_REGION = "us-east-1"
    CHARSET = "UTF-8"
    SUBJECT = "Reset your Civid password"
    client = boto3.client('ses', region_name=AWS_REGION)
    link = (uuid.uuid4().hex+user.verify_key.hex +
            uuid.uuid4().hex).replace('-', '').upper()

    BODY_HTML = f"""<html>
    <head></head>
    <body>
    <h3>Verify your email address</h3>
    <a href="{settings.SITE_URL}/verify-email/{link}">Click here</a> or follow the link below to verify your email address so that we can keep your account secure and send further emails.
    <br/><br/>
    <a href="{settings.SITE_URL}/verify-email/{link}">{settings.SITE_URL}/verify-email/{link}</a>
    </body>
    </html>
    """
    BODY_TEXT = f""""""
    try:
        # Provide the contents of the email.
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    user.email,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': BODY_HTML,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            ReturnPath='cividbot@civid.com',
            Source=SENDER,
        )

    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])

