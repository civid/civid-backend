from django.contrib.auth import authenticate
from django.shortcuts import get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from v1.accounts.models.user import User
from v1.accounts.serializers.user import (UserSerializer,
                                          UserSerializerCreate,
                                          UserSerializerLogin)
from v1.accounts.models.profile import Profile
from .profile import ProfileSerializer
from ...utils import constants


def unpack_errors(errors):
    message = []

    for key in errors.keys():
        message.append(errors[key])
    return message


class SignUpView(APIView):
    authentication_classes = ()
    permission_classes = ()

    @staticmethod
    def post(request):
        """Get user data and API token."""

        email = request.data.get("email", '')

        try:
            email = User.objects.get(email=email)
            return Response({'message': "A user with this email already exists. Please login"}, status=status.HTTP_400_BAD_REQUEST)
        except ObjectDoesNotExist:
            # create user
            pass
            serializer = UserSerializerCreate(data=request.data,
                                              context={'request': request})

            if serializer.is_valid():
                pass
                user = serializer.save()
                user.set_password(serializer.validated_data['password'])
                user.send_verification_email()
                user.save()
                Profile(user=user).save()
                return Response(UserSerializerLogin(user).data, status=status.HTTP_201_CREATED)
            else:
                return Response({'message': unpack_errors(serializer.errors)}, status=status.HTTP_400_BAD_REQUEST)
"""
        # login user
        email = request.data.get('email')
        password = request.data.get('password')
        user = get_object_or_404(User, email=email)
        user = authenticate(username=user.username,
                            password=password
                            )
        print("test")
        if user:
            serializer = UserSerializerLogin(user)
            return Response(serializer.data)
        print("test2")

        return Response(status=status.HTTP_400_BAD_REQUEST)
"""
