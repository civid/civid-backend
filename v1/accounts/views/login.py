from django.contrib.auth import authenticate
from django.shortcuts import get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from v1.accounts.models.user import User
from v1.accounts.serializers.user import UserSerializerLogin


# login
class LoginView(APIView):
    authentication_classes = ()
    permission_classes = ()

    @staticmethod
    def post(request):
        """Get user data and API token."""

        username = request.data.get("username", '')
        if "@" not in username:
            try:
                username = request.data.get("username").strip()
                email = User.objects.get(username__iexact=username).email
            except ObjectDoesNotExist:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            email = request.data.get('username').strip()

        password = request.data.get('password')
        user = get_object_or_404(User, email__iexact=email)
        user = authenticate(username=user.email,
                            password=password
                            )
        if user:
            serializer = UserSerializerLogin(user)
            return Response(serializer.data)
        return Response(status=status.HTTP_400_BAD_REQUEST)
