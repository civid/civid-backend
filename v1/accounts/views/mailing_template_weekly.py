# todo, make this an app in admin where email templates can be created
# edited and send to users

from django.conf import settings
import uuid
import boto3
from botocore.exceptions import ClientError


def mailing_template(user, queue):
    # todo , create a log error

    max_posts = 7
    post_html = ''
    for q in queue[0:max_posts]:
        post_html += f"<h1>{q.post.title}</h1>"
        post_html += q.post.claim_0
        if q.post.claim_1:
            post_html += q.post.claim_1
        if q.post.claim_2:
            post_html += q.post.claim_2
        if q.post.claim_3:
            post_html += q.post.claim_3

    SENDER = "Civid <cividbot@civid.com>"
    AWS_REGION = "us-east-1"
    CHARSET = "UTF-8"
    SUBJECT = "Mailing list"
    client = boto3.client('ses', region_name=AWS_REGION)

    body_html = f"""<html>
    <head></head>
    <body>
    <h3>Hello {user.username}</h3>
    TODO: Insert upcoming topics here (weekly? how many topics)
    """

    body_html += post_html

    body_html += f"""
    <a href="{settings.SITE_URL}remove-from-mailing/{user.id}">Remove me from this malling list</a>

    </body>
    </html>
    """

    try:
        # Provide the contents of the email.
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    user.email,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': body_html,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': '',
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            Source=SENDER,
        )

    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])
