from django.conf import settings
import uuid
import boto3
from botocore.exceptions import ClientError


def verification_email(user=''):
    # todo , create a log error

    SENDER = "Civid <noreply@civid.com>"
    AWS_REGION = "us-east-1"
    CHARSET = "UTF-8"
    SUBJECT = "Verify your Civid email address"
    client = boto3.client('ses', region_name=AWS_REGION)
    link = (uuid.uuid4().hex+user.verify_key.hex +
            uuid.uuid4().hex).replace('-', '').upper()

    BODY_HTML = f"""
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" lang="eng" xml:lang="eng">
  <head>
  <title>Civid</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;" />
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />

  </head>
  <body style="margin:0; padding:0; height:100%; width:100%" height="100%" width="100%">
    <table cellpadding="0" cellspacing="0" border="0" id="background-table" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; background-color:#f9f9f9; margin:0; padding:0; height:100%; width:100%" bgcolor="#f9f9f9" height="100%" width="100%">
      <tr>
        <td class="background-table-td" align="center" valign="top" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly">

          <!-- center white frame -->
          <table class="centered-table-container" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; margin:0 10px; width:560px" width="560">

            <!-- empty padding top -->
            <tr>
              <td class="background-padding-top" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; height:20px" height="20">&#160;</td>
            </tr>

            <!-- logo block container -->
            <tr>
              <td style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly">
                <table class="main-container-table" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly">
                  <tr>
                    <td class="logo-container-block" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; border:1px solid #efefef">
                      <table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; background-color:#fff; width:100%" bgcolor="#ffffff" width="100%">
                        <!-- white padding above logo -->
                        <tr>
                          <td class="logo-padding-top" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; height:40px" height="40">&#160;</td>
                        </tr>
                        <tr>
                          <td align="center" class="civid-logo-container" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; width:560px" width="560">
                            <!-- rem nested table if wrapping with a link works in gmail android-->
                            <table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly">
                              <tr>
                                <td style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly">

                                    <a class="civid-logo" href="https://www.civid.com" style="color:#2854a1; font-size:21px; margin:0; padding:0">

      <img src="https://s3.amazonaws.com/civid-media/static/iconlogo-2x.png" alt="Civid" class="civid-logo-image civid-logo-image--default" style="border:none; display:block; outline:0; text-decoration:none; width:100%; max-width:120px" width="100%" />

  </a>

                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td class="logo-padding-bottom" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; height:40px" height="40">&#160;</td>
                        </tr>
                      </table>
                    </td>
                  </tr>

                  <!-- email body -->

                  <tr>
                    <td class="body-container-block" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; border:1px solid #efefef">
                      <table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; background-color:#fff; width:100%" bgcolor="#ffffff" width="100%">
                        <tr>
                          <!-- left padding -->
                          <td class="body-padding-side" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; width:35px; text-align:left; -webkit-font-smoothing:antialiased" width="35" align="left">&#160;</td>
                          <!-- inner body container -->
                          <td class="body-container-td" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; text-align:left; -webkit-font-smoothing:antialiased" align="left">
                            <table class="body-container-td-table" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly">
                              <tr>
                                <td class="body-padding-top" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; height:40px; text-align:left; -webkit-font-smoothing:antialiased" height="40" align="left">&#160;</td>
                              </tr>
                              <tr>
                                <td class="body-header" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; color:#003bde; font-size:24px; padding-bottom:25px; text-align:left; -webkit-font-smoothing:antialiased" align="left">

  Welcome to Civid!

                                </td>
                              </tr>

  <tr>
    <td class="body-text" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; color:#5c6b7e; font-size:14px; padding-bottom:28px; text-align:left; -webkit-font-smoothing:antialiased; line-height:21px" align="left">
        The only platform where meaningful discussions take place.
      <br/>
      <br/>
      We’re excited to have you as part of our community.
    </td>
  </tr>
   <tr>
                          <tbody>
                            <tr>
                              <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;" valign="top">
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
                                  <tbody>
                                    <tr>
                                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; border-radius: 5px; text-align: center; background-color: #3498db;" valign="top" align="center" bgcolor="#3498db"> <a href="{settings.SITE_URL}/verify-email/{link}" target="_blank" style="border: solid 1px #3081f9; border-radius: 5px; box-sizing: border-box; cursor: pointer; display: inline-block; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-decoration: none; text-transform: capitalize; background-color: #3081f9; border-color: #3081f9; color: #ffffff;">Confirm email address</a> </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>

  </tr>



                                <tr>
                                  <td class="body-padding-bottom" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; height:40px; text-align:left; -webkit-font-smoothing:antialiased" height="40" align="left">&#160;</td>
                                </tr>

                            </table>
                          </td>
                          <!-- right padding -->
                          <td class="body-padding-side" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; width:35px; text-align:left; -webkit-font-smoothing:antialiased" width="35" align="left">&#160;</td>
                        </tr>
                      </table>
                    </td>
                  </tr>



                    <!-- contact us -->
                    <tr>
                      <td class="contact-container-block" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; border:1px solid #efefef">

                      </td>
                    </tr>


                </table>
              </td>
            </tr>

            <!-- drop show container row -->
            <tr>
              <td class="drop-show-row" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; height:20px" height="20">&#160;</td>
            </tr>

            <!-- drop show bottom padding -->
            <tr>
              <td class="drop-show-padding-bottom" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; height:20px" height="20">&#160;</td>
            </tr>

            <!-- co-branding signature area -->

            <!-- START FOOTER -->
            <div class="footer" style="clear: both; margin-top: 10px; text-align: center; width: 100%;">
              <table role="presentation" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                <tr>
                  <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; color: #999999; font-size: 12px; text-align: center;" valign="top" align="center">
                    <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;">Sent by Civid, inc.</span>
                  </td>
                </tr>
              </table>
            </div>
            <!-- END FOOTER -->



            <tr>
              <td class="background-bottom-padding" style="border-collapse:collapse; font-family:&quot;Avenir Next&quot;, &quot;Open Sans&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif; mso-line-height-rule:exactly; height:40px" height="40">&#160;</td>
            </tr>

          </table>
        </td>
      </tr>
    </table>
  </body>
</html>



    """
    BODY_TEXT = f""""""
    try:
        # Provide the contents of the email.
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    user.email,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': BODY_HTML,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            ReturnPath='cividbot@civid.com',
            Source=SENDER,
        )

    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])

