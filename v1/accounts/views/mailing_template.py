# todo, make this an app in admin where email templates can be created
# edited and send to users

from django.conf import settings
import uuid
import boto3
from botocore.exceptions import ClientError
import datetime


def mailing_template(user, post):
    # todo , create a log error

    max_posts = 7
    post_html = ''

    post_html += f"""<img src="https://civid-media.s3.amazonaws.com/media/{post.image}" style="max-height:200px"/>"""
    post_html += f"<h1>{post.title}</h1>"
    post_html += post.user.username
    post_html += f"<br>{datetime.datetime.today().strftime('%Y-%m-%d')}"
    post_html += f"""<p>Join the discussion <a href="{settings.SITE_URL}/posts/{post.slug}">here</a>"""

    SENDER = "Civid <cividbot@civid.com>"
    AWS_REGION = "us-east-1"
    CHARSET = "UTF-8"
    SUBJECT = "Today's Discussion"
    client = boto3.client('ses', region_name=AWS_REGION)

    body_html = f"""<html>
    <head></head>
    <body>
    <h3>Today's discussion on Civid</h3>
    """

    body_html += post_html

    body_html += f"""
    <br><br><br> <a href="{settings.SITE_URL}/remove-from-mailing/{user.id}" style="text-decoration: underline; color: #999999; font-size: 12px; text-align: center;">Unsubscribe</a>
    </body>
    </html>
    """

    try:
        # Provide the contents of the email.
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    user.email,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': body_html,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': '',
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            Source=SENDER,
        )

    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])
