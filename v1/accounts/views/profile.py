from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import serializers
from v1.accounts.models.profile import Profile
from v1.accounts.serializers.profile import ProfileSerializer, ProfileSerializerUpdate
from v1.accounts.serializers.user import UserSerializerLogin


# profiles
class ProfileView(APIView):

    @staticmethod
    def get(request):
        """List profiles."""
        profiles = Profile.objects.all()
        return Response(ProfileSerializer(profiles, many=True).data)


# profiles/{profile_id}
class ProfileDetail(APIView):

    @staticmethod
    def patch(request, profile_id):
        """Update profile of authenticated user."""
        profile = get_object_or_404(Profile, pk=profile_id)
        if profile.user != request.user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        serializer = ProfileSerializerUpdate(profile, data=request.data, context={'request': request}, partial=True)
        if serializer.is_valid():
            profile = serializer.save()
            return Response(UserSerializerLogin(profile.user).data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# profiles/{profile_id}/follow
class ProfileFollowView(APIView):

    # follow
    def post(self, request, profile_id):
        follower = self.request.user.profile
        followee = get_object_or_404(Profile, pk=profile_id)
        if follower.pk is followee.pk:
            raise serializers.ValidationError('You can not follow yourself.')
        follower.follow(followee)
        return Response(ProfileSerializer(follower).data, status=status.HTTP_201_CREATED)


    # unfollow
    def delete(self, request, profile_id):
        follower = self.request.user.profile
        followee = get_object_or_404(Profile, pk=profile_id)
        follower.unfollow(followee)
        return Response(None, status=status.HTTP_204_NO_CONTENT)


# profiles/{profile_id}/followers
class ProfileFollowersView(APIView):

    @staticmethod
    def get(request, profile_id):
        """List followers."""
        profile = get_object_or_404(Profile, pk=profile_id)
        followers = Profile.objects.filter(follows=profile)
        return Response(ProfileSerializer(followers, many=True).data)


# profiles/{profile_id}/following
class ProfileFollowingView(APIView):

    @staticmethod
    def get(request, profile_id):
        """List following."""
        profile = get_object_or_404(Profile, pk=profile_id)
        following = Profile.objects.filter(user=profile.user)
        return Response(ProfileSerializer(following, many=True).data)
