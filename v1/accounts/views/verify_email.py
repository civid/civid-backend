import uuid
import datetime
from django.utils.timezone import make_aware
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from ..models.user import User
from rest_framework.response import Response
from rest_framework.views import APIView
from v1.utils import constants
from ..views.verification_email import verification_email


class VerifyEmailView(APIView):
    permission_classes = ()

    @staticmethod
    def post(request, id):
        """ Verify email."""

        verify = uuid.UUID(id[32:64])

        try:
            user = User.objects.get(verify_key=verify)
            user.email_verified = True
            user.mailing_opt_in = True
            user.verify_date = make_aware(datetime.datetime.now())
            user.verify_key = uuid.uuid4()
            user.save()
            return Response("Ok", status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            pass

        return Response("Email couldn't be verified.", status=status.HTTP_428_PRECONDITION_REQUIRED)


class VerifyEmailSendView(APIView):
    permission_classes = ()

    @staticmethod
    def post(request, id):
        """Send Verify email on demand."""

        try:
            user = User.objects.get(id=id)
            user.send_verification_email()
            return Response("Ok", status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            pass

        return Response("Email couldn't be verified.", status=status.HTTP_428_PRECONDITION_REQUIRED)
