from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from ..models.user import User
from rest_framework.response import Response
from rest_framework.views import APIView
from v1.utils import constants
from ..views.verification_email import verification_email


class RemoveUserFromMailingView(APIView):
    permission_classes = ()

    @staticmethod
    def post(request, id):
        """ Remove user from mailing."""

        try:
            user = User.objects.get(id=id)
            user.mailing_opt_in = False
            user.save()
            return Response("Ok", status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            pass

        return Response("User not found.", status=status.HTTP_428_PRECONDITION_REQUIRED)
