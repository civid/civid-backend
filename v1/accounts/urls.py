from django.conf.urls import url
from .views.accept_invitation import AcceptInvitationView
from .views.login import LoginView
from .views.logout import LogoutView
from .views.profile import (ProfileView,
                            ProfileDetail,
                            ProfileFollowView,
                            ProfileFollowersView,
                            ProfileFollowingView)
from .views.reset_password import (ResetPasswordView,
                                   ResetPasswordSendView)
from .views.update_password import UpdatePasswordView
from .views.user import UserView, UserDetail
from .views.signup import SignUpView
from .views.verify_email import (VerifyEmailView,
                                 VerifyEmailSendView)
from .views.remove_from_mailing import RemoveUserFromMailingView

urlpatterns = [

    # Accept invitation
    url(r'^accept_invitation$', AcceptInvitationView.as_view()),

    # Login / logout
    url(r'^login$', LoginView.as_view()),
    url(r'^logout$', LogoutView.as_view()),

    # Password management
    url(r'^reset_password$', ResetPasswordView.as_view()),
    url(r'^send_reset_password$', ResetPasswordSendView.as_view()),
    url(r'^update_password$', UpdatePasswordView.as_view()),

    # Profiles
    url(r'^profiles$', ProfileView.as_view()),
    url(r'^profiles/(?P<profile_id>[\d]+)$', ProfileDetail.as_view()),
    #url(r'^profiles/(?P<profile_id>[\d]+)/follow/?$',
    #    ProfileFollowView.as_view()),
    #url(r'^profiles/(?P<profile_id>[\d]+)/followers/?$',
    #    ProfileFollowersView.as_view()),
    #url(r'^profiles/(?P<profile_id>[\d]+)/following/?$',
    #    ProfileFollowingView.as_view()),

    # Users
    url(r'^users$', UserView.as_view()),
    url(r'^users/(?P<username>[\w-]+)/$', UserDetail.as_view()),
    url(r'^sign-up$', SignUpView.as_view()),

    # verify email
    url(r'^verify-email/(?P<id>[0-9A-Fa-f-]+)$', VerifyEmailView.as_view()),
    url(r'^send-verify-email/(?P<id>[\d]+)', VerifyEmailSendView.as_view()),

    # remove user from  mailing list
    url(r'^remove-from-mailing/(?P<id>[\d]+)',
        RemoveUserFromMailingView.as_view()),

]
