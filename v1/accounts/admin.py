from django.contrib import admin
from .models.profile import Profile
from .models.reset_password_code import ResetPasswordCode
from .models.user import User


class UserAdmin(admin.ModelAdmin):
    exclude = ('groups', 'user_permissions')
    list_display = ('username', 'get_avatar', 'email', 'email_verified',
                    'first_name', 'last_name', 'is_staff', 'is_active',
                    'mailing_opt_in', 'influence', 'description', 'location',
                    'work', 'education', 'twitter', 'about_text'
                    )


admin.site.register(User, UserAdmin)


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'get_image')


admin.site.register(Profile, ProfileAdmin)

admin.site.register(ResetPasswordCode)
