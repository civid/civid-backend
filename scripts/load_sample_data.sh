#!/bin/bash

# Accounts
python3 manage.py loaddata v1/accounts/fixtures/user.json
python3 manage.py loaddata v1/accounts/fixtures/profile.json
python3 manage.py loaddata v1/accounts/fixtures/token.json

# Articles
python3 manage.py loaddata v1/articles/fixtures/article.json
python3 manage.py loaddata v1/articles/fixtures/publication.json
python3 manage.py loaddata v1/articles/fixtures/source.json

# Posts
python3 manage.py loaddata v1/posts/fixtures/post.json

# User roles
python3 manage.py loaddata v1/user_roles/fixtures/administrator.json
python3 manage.py loaddata v1/user_roles/fixtures/moderator.json
