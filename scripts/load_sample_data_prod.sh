#!/bin/bash

# Accounts
python3 manage.py loaddata v1/accounts/fixtures/user_prod.json
python3 manage.py loaddata v1/accounts/fixtures/profile_prod.json
python3 manage.py loaddata v1/accounts/fixtures/token_prod.json

# User roles
python3 manage.py loaddata v1/user_roles/fixtures/administrator.json
