## Project Setup

Install required packages:
```
pip install -r requirements/local.txt
```
## PostgreSQL

Create a database:
```
psql
CREATE USER sample_user WITH PASSWORD 'sample_password';
CREATE DATABASE civid OWNER sample_user;
```

## Environment variables

Create a .env file in the root directory with the following:
```
SECRET_KEY=your_secret_key
POSTGRES_NAME=civid
POSTGRES_USER=sample_user
POSTGRES_PASSWORD=sample_password
POSTGRES_HOST=localhost
POSTGRES_PORT=5432
```

Initialize database:
```
python3 manage.py makemigrations
python3 manage.py migrate
```

## Fixtures

To load in sample data for all tables at once:
```
bash scripts/load_sample_data.sh
```

For Windows users:
```
python3 manage.py loaddata v1\accounts\fixtures\user.json
```

A superuser account with the following credentials will be created:
```
admin@email.com
pass1234
```
This can be used to access the admin panel at localhost:8000/admin.
